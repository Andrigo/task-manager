import React from 'react';
import Navbar from '../navigation/Navbar.js';
import DateBox from '../components/DateBox.js';
import TaskBox from '../components/TaskBox.js';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default class Home extends React.Component {
  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
       <Navbar />
       <DateBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
       <TaskBox />
      </View>
      </ ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';


export default class Navbar extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        
        <Text style={styles.text}>Task Manager</Text>
        <Image style={styles.img} source={require('../assets/img/add.png')} />

      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#655cd9',
    width: '100%',
    justifyContent: 'center',
    height: 100
  },
  text: {
    textAlign: 'center',
    color: 'white',
    fontSize: 23,
    fontWeight: '900',
  },
  img: {
    width: 20,
    height: 20,
    alignSelf: 'flex-end',
    marginTop: '-6%',
    marginRight: '7%'
  }
});
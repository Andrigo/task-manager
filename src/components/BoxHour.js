import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';


export default class TaskBox extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.texthourinit}>
            21
       </Text> 
       <Text style={styles.texthourpoint}>
            . .
       </Text> 
       <Text style={styles.texthourend}>
            09
       </Text> 
      </View>
    );
  }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#b3c4db',
      width: 60,
      justifyContent: 'center',
      height: 60,
      borderRadius: 21,
      marginLeft: 25,
    },
    texthourinit: {
      textAlign: 'center',
      color: 'white',
      fontSize: 18,
      backgroundColor: 'transparent',
      marginBottom: -10
    },
    texthourpoint: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        backgroundColor: 'transparent'
      },
      texthourend: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        backgroundColor: 'transparent'
      },
   
  });


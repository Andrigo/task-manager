import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';


export default class TaskBox extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.textup}>
            Lorem ipsum
        </Text>
        <Text style={styles.textdown}>
            Lorem ipsum dolor sit asimet
        </Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'transparent',
      marginLeft: 10
    },
    textup: {
      color: 'gray',  
      textAlign: 'left',
      fontSize: 18,
    },
    textdown: {
        color: 'gray',          
        textAlign: 'left',
        fontSize: 12,
      },
    
  });
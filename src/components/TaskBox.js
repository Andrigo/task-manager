import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';
import BoxHour from '../components/BoxHour.js';
import BoxText from '../components/BoxText.js';


export default class TaskBox extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        <BoxHour />
        <BoxText />
        <Image style={styles.img} source={require('../assets/img/arrow-right.png')} />

      </View>
    );
  }

}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        marginTop: '14%',
        marginBottom: -25,
        

    },
    img: {
        width: 30,
        height: 30,
        marginLeft: 20,

    }
  });

import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';


export default class DateBox extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.text}>
            Hoje
        </Text>
      </View>
    );
  }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'transparent',
    },
    text: {
      color: 'gray',  
      textAlign: 'center',
      fontSize: 14,
      marginTop: 5,
      marginBottom: -20,
    },
    
  });